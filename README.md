# Boat Ramps Visualizer

`ramps-visualizer` is a simple application for users to visualize geo-locations of boat ramps in Australia's Gold-Coast delivered using a simple client-server architecture approach<p>
It is developed using: node-js, express-js, react+redux

## Directory Structure

### Server

1.  `server.js`

- Defines dependencies and initializes a http server

2.  `config/default.json`

- Defines key variables used throughout the server

3. `src/routes/data.js`

- Defines routes using the RESTful convention and their corresponding route handlers

4. `src/controllers/dataController.js`

- Defines and exports the functions that are executed as route handlers

5. `src/data/boat_ramps.json`

- Contains the GeoJSON data points that are sent to the client

### Client

1.  `src/app.js`

- Initializes the main App component and renders the Map component

2.  `src/map.js`

- Renders the MapBox Map onto the viewport and defines event-listeners and event-handlers

3. `src/infoPanel.js`

- Renders an information pop-up containing additional information on a particular ramp

4. `src/helpers.js`

- Defines and exports the helper functions that are commonly used throughout the client

5. `src/redux`

- Defines the actions and reducers that interact with, and modify the Redux store

## Installation and Usage

### Client

1. Clone the repository
2. Run `yarn install`
3. Start the client using `yarn run start`

- The client will start on port 8080

### Server

1. Clone the repository
2. Run `yarn install`
3. Start the server using `yarn run start`

- The server will start on port 30000

### Server Endpoints

- The server is built to follow RESTful conventions wherein the client can ask for the entirety of objects belonging to a particular class or a single object by specifying its :id

- To mock a real-world scenario, the server accepts an API-key to authorize requests.

#### /api/v1/data/ramps (GET)

Returns all of the boat ramps as Geo-JSON objects

Parameters:

```
{
    "x-api-key" :: String,
}
```

Response 200:

```
{
    "message" : "Request successful",
    "data" :: DATA
}
```

Response 401 (Unauthorized):

```
{
    "message" : "Invalid API-key, please check your key"
}
```

Response 500:

```
{
    "message" : "Internal Server Error",
    "error" :: Error
}
```

#### /api/v1/data/ramps/:id (GET)

Returns a single boat ramp Geo-JSON object based on the value of id supplied by the client

Parameters:

```
{
    "x-api-key" :: String,
    "rampId" :: ObjectId
}
```

Response 200:

```
{
    "message" : "Request successful",
    "data" :: DATA
}
```

Response 401 (Unauthorized):

```
{
    "message" : "Invalid API-key, please check your key"
}
```

Response 404 (Not Found):

```
{
    "message" : "Invalid id, ramp not found, please re-check your id"
}
```

Response 500:

```
{
    "message" : "Internal Server Error",
    "error" :: Error
}
```

### Security Considerations

- For the sake of the challenge, the API key for the backend and the Mapbox Token are stored in a `credentials.json` file. These credentials will usually never be stored on the front-end and instead be handed by a service from the backend to the client where-ever needed.

- Simple API key authorization is carried out with a set of pre-defined keys to simulate how a client and server would communicate using this convention

## To Do

1. A map to be able to visualise all the boat ramps.
2. A data visualisation of your choice that displays the number or ramps per construction material.
3. A data visualisation of your choice that displays the number of ramps per size category (values of area in 3 different ranges: [0, 50), [50, 200), and [200, 526)).
4. Zooming in the map should filter the visualised data to include only those ramps which are currently visible in the viewport.
5. Clicking on a data point on a visualisation, should filter the ramps on the map to reflect the selected data.

## Final Result

![Image description](https://i.imgur.com/V6CINVQ.jpg)

1. User is able to view and navigate freely between all boat ramps on the map
2. User is able to visualize the frequency of the ramps present by area and material on the viewport using a side-panel
3. The data inside side-panel alters dynamically depending on the ramps in the viewport
4. User is able to get further information on a ramp by clicking on a geo-pin
