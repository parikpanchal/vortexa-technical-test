// Import Libraries
import React, { Component } from "react";
import styled from "styled-components";

import MapGL, { Marker, Popup } from "react-map-gl";

import Info from "./infoPanel";

import { inBounds } from "./helpers";
import axios from "axios";

import viewChange from "./redux/actions";
import { connect } from "react-redux";

import credentials from "../credentials.json";

class Map extends Component {
  mapRef = null;
  state = {
    // store all geo-points
    data: {
      features: []
    },
    viewport: {
      latitude: -28,
      longitude: 153,
      zoom: 9,
      bearing: 0,
      pitch: 0
    },
    popupInfo: null,
    status: "",
    type: "",
    area: 0,
    material: "",
    comments: ""
  };

  // Fetch and load data into store
  componentDidMount() {
    axios
      .get(
        `http://localhost:30000/api/v1/data/ramps?x-api-key=${credentials.dev.SERVER_API_KEY}`
      )
      .then(res => {
        this._loadData(res.data);
      });
  }

  // set the redux store state with all of the geo-points
  _loadData = data => {
    this.setState({ data });
    this.props.viewChange(data.features);
  };

  // triggered when the user zooms || navigates through the map or in or out
  _onViewportChange = viewport => {
    this.setState({ viewport });
    let points = [];
    // extract the geo-json points from redux
    const { data } = this.state;
    if (!this.mapRef) return;
    const bounds = this.mapRef.getMap().getBounds();

    data.features.map(point => {
      if (inBounds(point, bounds)) {
        points.push(point);
      }
    });

    this.props.viewChange(points);
  };

  // fill info to popup and set in state
  onClickPoint = point => {
    const popupInfo = {
      latitude: point.geometry.coordinates[0][0][0][1],
      longitude: point.geometry.coordinates[0][0][0][0]
    };

    this.setState({
      popupInfo,
      status: point.properties.status,
      type: point.properties.type,
      area: point.properties.area_,
      material: point.properties.material,
      comments: point.properties.comments
    });
  };

  render() {
    const {
      viewport,
      data,
      popupInfo,
      status,
      type,
      area,
      material,
      comments
    } = this.state;

    return (
      <MapContainer>
        <MapGL
          ref={ref => (this.mapRef = ref)}
          {...viewport}
          width="100%"
          height="100%"
          mapStyle="mapbox://styles/mapbox/light-v9"
          onViewportChange={this._onViewportChange}
          mapboxApiAccessToken={credentials.dev.MAPBOX_TOKEN}
        >
          {data.features.map((point, index) => {
            return (
              <div key={index} onClick={() => this.onClickPoint(point)}>
                <Marker
                  longitude={point.geometry.coordinates[0][0][0][0]}
                  latitude={point.geometry.coordinates[0][0][0][1]}
                >
                  <ImageContainer src="https://i.imgur.com/4ltlnbF.png"></ImageContainer>
                </Marker>
              </div>
            );
          })}

          {popupInfo && (
            <Popup
              longitude={popupInfo.longitude}
              latitude={popupInfo.latitude}
              closeOnClick={false}
              onClose={() => this.setState({ popupInfo: null })}
            >
              <h3>Ramp Information</h3>
              <p>Status: {status}</p>
              <p>Material: {material}</p>
              <p>Type: {type}</p>
              <p>Area: {area}</p>
              {comments ? <p>Comments: {comments}</p> : <p></p>}
            </Popup>
          )}
        </MapGL>
        <Info />
      </MapContainer>
    );
  }
}

// Styled Components
const MapContainer = styled.div`
  height: 100%;
`;

const ImageContainer = styled.img`
  width: 20px;
  height: auto;
`;

// Exports
export default connect(null, { viewChange })(Map);
