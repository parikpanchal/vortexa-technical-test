// return count of points
const filterPoints = mapPoints => {
  return {
    concreteCount: mapPoints.filter(
      point => point.properties.material === "Concrete"
    ).length,

    bitumenCount: mapPoints.filter(
      point => point.properties.material === "Bitumen"
    ).length,

    gravelCount: mapPoints.filter(
      point => point.properties.material === "Gravel"
    ).length,

    othersCount:
      mapPoints.length -
      (mapPoints.filter(point => point.properties.material === "Concrete")
        .length -
        mapPoints.filter(point => point.properties.material === "Bitumen")
          .length -
        mapPoints.filter(point => point.properties.material === "Gravel")
          .length),

    zeroToFiftyAreaCount: mapPoints.filter(
      point => point.properties.area_ >= 0 && point.properties.area_ < 50
    ).length,

    fiftyToTwoHundredAreaCount: mapPoints.filter(
      point => point.properties.area_ >= 50 && point.properties.area_ < 200
    ).length,

    greaterThanTwoHundredAreaCount: mapPoints.filter(
      point => point.properties.area_ >= 200
    ).length
  };
};

// extract longitude and latitude from json object
inBounds = (point, bounds) => {
  var lng =
    (point.geometry.coordinates[0][0][0][0] - bounds._ne.lng) *
      (point.geometry.coordinates[0][0][0][0] - bounds._sw.lng) <
    0;
  var lat =
    (point.geometry.coordinates[0][0][0][1] - bounds._ne.lat) *
      (point.geometry.coordinates[0][0][0][1] - bounds._sw.lat) <
    0;
  return lng && lat;
};

// Exports
module.exports = {
  filterPoints,
  inBounds
};
