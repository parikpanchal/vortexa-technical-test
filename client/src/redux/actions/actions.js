export const viewChange = params => {
  return {
    type: "VIEW_CHANGE",
    payload: params
  };
};
