// Import Libraries
import React, { Component } from "react";
import { connect } from "react-redux";

import { filterPoints } from "./helpers";
import styled from "styled-components";

class InfoPanel extends Component {
  render() {
    const data = filterPoints(this.props.mapPoints);

    return (
      <Panel>
        <h3>Ramps Information</h3>
        <p>By material</p>
        <span>Concrete: {data.concreteCount}</span>
        <br />
        <span>Bitumen: {data.bitumenCount}</span>
        <br />
        <span>Gravel: {data.gravelCount}</span>
        <br />
        <span>Others: {data.othersCount}</span>
        <br />
        <p>By Area</p>
        <span>0 to 50: {data.zeroToFiftyAreaCount}</span>
        <br />
        <span>50 to 200: {data.fiftyToTwoHundredAreaCount}</span>
        <br />
        <span>200 to 565: {data.greaterThanTwoHundredAreaCount}</span>
        <br />
      </Panel>
    );
  }
}

// Styled Components
const Panel = styled.div`
  position: absolute;
  top: 30%;
  left: 0;
  max-width: 320px;
  width: 150px;
  padding: 20px;
  background: rgba(0, 0, 0, 0.5);
  color: white;
  font-size: 13px;
  line-height: 2;
  h3 {
    font-weight: 800;
    text-align: center;
  }
`;

// Exports
export default connect(state => ({ mapPoints: state.mapPoints }))(InfoPanel);
