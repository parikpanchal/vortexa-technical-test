// import libraries
const config = require("config");
const keys = config.get("app").sampleAPIKeys;

// import data
const data = require("../data/boat_ramps");

module.exports.fetchRamps = (req, res) => {
  try {
    const key = req.query["x-api-key"];
    if (keys.includes(key)) {
      return res.status(200).send(data);
    } else {
      return res
        .status(401)
        .send("message: Invalid API-key, please check your key");
    }
  } catch (e) {
    return res.status(500).send(e);
  }
};

module.exports.fetchRamp = (req, res) => {
  try {
    const key = req.query["x-api-key"];
    const id = req.params.id;
    if (keys.includes(key)) {
      console.log(id);
      dataFeatures = data.features;
      for (let i = 0, n = dataFeatures.length; i < n; i++) {
        if (dataFeatures[i]["id"] === id) {
          return res.status(200).send(dataFeatures[i]);
        } else {
          return res
            .status(404)
            .send(
              "message: Invalid id, ramp not found, please re-check your id"
            );
        }
      }
    } else {
      return res
        .status(401)
        .send("message: Invalid API-key, please check your key");
    }
  } catch (e) {
    return res.status(500).send(e);
  }
};
