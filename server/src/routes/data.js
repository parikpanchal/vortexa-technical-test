// import libraries
const DataController = require("../controllers/dataController");
const express = require("express");
const router = new express.Router();

// get all boat ramp points
router.get("/api/v1/data/ramps", (req, res) =>
  DataController.fetchRamps(req, res)
);

// get a single boat ramp point
router.get("/api/v1/data/ramps/:id", (req, res) =>
  DataController.fetchRamp(req, res)
);

// exports
module.exports = router;
