// import libraries
const express = require("express");
const config = require("config");
const cors = require("cors");

const port = config.get("app").port;

const app = express();

// set router
const dataRouter = require("./src/routes/data");

// set server constants
app.use(cors());
app.use(dataRouter);

// start server
app.listen(port, () => {
  console.log(`Server starting on port ${port}`);
});
